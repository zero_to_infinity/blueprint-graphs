'use strict';

/* Directives */

angular.module('blueprint.directives', [])

    .directive('jsonViewer', function (dataService) {
        return {
            scope : {
                json : "=",
                onSelect : "="
            },
            link: function postLink(scope, element, attrs) {
                scope.$watch('json', function(json){
                    update();
                })
                function update(){
                    d3.select(element[0]).selectAll("*").remove();
                    var tree = d3.select(element[0])
                        .append("div")
                        .classed("json-node","true")
                    var j = scope.json;
                    explore(j, tree);
                    function explore(m, el){
                        if ( el === tree && is.object(m) && is.not.array(m) && is.not.empty(m) ) {
                            el.append("div")
                            //  .classed("json-node","true")
                                .text(function(d){
                                    return "{";
                            })
                        }
                        var n = el === tree && is.array(m) && is.not.empty(m) ? [m] : m;
                        for (var c in n) {
                            var cel = el.append("div")
                                .datum(n[c])//function(d){console.log(el === tree, n); return el === tree ? {tree:n} : n[c]})
                                .classed("json-node","true")
                            if ( is.array(n[c]) && is.not.empty(n[c])) {
                                cel.classed("json-closed", function(d){ return el === tree ? "false" : "true"})
                                cel.classed("json-array", function(d){ return el === tree ? "false" : "true"})
                                //data-toggle="tooltip"
                                //data-title="Clear all"
                                cel.append("i")
                                .classed("json-icon fa fa-plus-square-o pull-left","true")
                                .on("click", function(d){
                                    d3.event.stopPropagation();
                                    d3.select(this.parentNode).classed("json-closed", function(){
                                        return !d3.select(this).classed("json-closed");
                                    })
                                    d3.select(this).classed("fa-plus-square-o", d3.select(this.parentNode).classed("json-closed"))
                                    d3.select(this).classed("fa-minus-square-o", !d3.select(this.parentNode).classed("json-closed"))
                                })
                            }
                            cel.append("div")
                            .html(function(d){
                                    var pre = is.array(n) ? "" : "<b>"+c + "</b> : ";
                                    var text = is.array(n[c]) ? "[" : is.object(n[c]) ? "{" : n[c];
                                    text += is.array(n[c]) && !n[c].length ? "]" : is.object(n[c]) && is.empty(n[c]) ? "}" : "";
                                    return pre + text;
                                })
                            if (is.object(n[c])) explore(n[c], cel);
                        }
                        if (is.array(n) && el !== tree) {
                            el.select('div')
                            .attr("data-toggle","tooltip")
                            .attr("data-title", function(d){
                                return "Load " + d.length + " records";
                            })
                            .on("mouseover", function(d){
                                d3.event.stopPropagation();
                                d3.select(this.parentNode).classed("json-hover", true)
                            })
                            .on("mouseout", function(d){
                                d3.event.stopPropagation();
                                d3.select(this.parentNode).classed("json-hover", false)
                            })
                            .on("click", function(d){
                                d3.event.stopPropagation();
                                scope.onSelect(d);
                            })
                        }
                        if ( is.object(n) && is.not.empty(n) ) {
                            if (is.array(n) && el === tree) return;
                            el.append("div")
                            //  .classed("json-node","true")
                                .text(function(d){
                                    var text = is.array(n) ? "]" : "}";
                                    return text;
                            })
                        }
                        $('[data-toggle="tooltip"]').tooltip({animation:false});
                    }
                }
            }
        };
    })
    
    .directive('chart', function ($rootScope, dataService) {
        return {
            restrict: 'A',
            link: function postLink(scope, element, attrs) {
                function update(){
                    $('*[data-toggle="tooltip"]').tooltip({ container:'body' });
                    d3.select(element[0]).select("*").remove();
                    if (!scope.chart || !scope.data.length) return;
                    if (!scope.model.isValid()) return;

                    d3.select(element[0])
                        .append("svg")
                        .datum(scope.data)
                        .call(scope.chart
                            .on('startDrawing', function() {
                                if(!scope.$$phase) {
                                    scope.chart.isDrawing(true)
                                    scope.$apply()
                                }
                            })
                            .on('endDrawing', function() {
                                $rootScope.$broadcast("completeGraph");
                                if(!scope.$$phase) {
                                    scope.chart.isDrawing(false)
                                    scope.$apply()
                                }
                            })
                        )

                    scope.svgCode = d3.select(element[0])
                        .select('svg')
                        .attr("xmlns", "http://www.w3.org/2000/svg")
                        .node().parentNode.innerHTML;

                    $rootScope.$broadcast("completeGraph");
                }

                scope.delayUpdate = dataService.debounce(update, 300, false);
                scope.$watch('chart', function(){ update(); });
                scope.$on('update', function(){ update(); });
                scope.$watch(function(){ if (scope.model) return scope.model(scope.data); }, update, true);
                scope.$watch(function(){ if (scope.chart) return scope.chart.options().map(function (d){ return d.value }); }, scope.delayUpdate, true);
            }
        };
    })

    .directive('chartOption', function () {
        return {
            restrict: 'A',
            link: function postLink(scope, element, attrs) {
                var firstTime = false;
                element.find('.option-fit').click(function(){
                    scope.$apply(fitWidth);
                });
                scope.$watch('chart', fitWidth);
                function fitWidth(chart, old){
                    if (chart == old) return;
                    if(!scope.option.fitToWidth || !scope.option.fitToWidth()) return;
                    scope.option.value = $('#chart').width();
                }
                $(document).ready(fitWidth);
            }
        };
    })

    .directive('sortable', function ($rootScope) {
        return {
            restrict: 'A',
            scope : {
                title : "=",
                value : "=",
                types : "=",
                multiple : "="
            },
            template:'<div class="msg">{{messageText}}</div>',
            link: function postLink(scope, element, attrs) {
                var removeLast = false;
                element.sortable({
                    items : '> li',
                    connectWith: '.dimensions-container',
                    placeholder:'drop',
                    start: onStart,
                    update: onUpdate,
                    receive : onReceive,
                    remove: onRemove,
                    over: over,
                    tolerance:'intersect'
                })

                function over(e,ui){
                    var dimension = ui.item.data().dimension,
                        html = isValidType(dimension) ? '<i class="fa fa-arrow-circle-down breath-right"></i>Drop here' : '<i class="fa fa-times-circle breath-right"></i>Don\'t drop here'
                    element.find('.drop').html(html);
                }

                function onStart(e,ui){
                    var dimension = ui.item.data().dimension,
                        html = isValidType(dimension) ? '<i class="fa fa-arrow-circle-down breath-right"></i>Drop here' : '<i class="fa fa-times-circle breath-right"></i>Don\'t drop here'
                    element.find('.drop').html(html);
                    element.parent().css("overflow","visible");
                    angular.element(element).scope().open=false;
                }

                function onUpdate(e,ui){
                    ui.item.find('.dimension-icon').remove();
                    if (ui.item.find('span.remove').length == 0) {
                        ui.item.append("<span class='remove pull-right'>&times;</span>")
                    }
                    ui.item.find('span.remove').click(function(){  ui.item.remove(); onRemove(); });
                    if (removeLast) {
                        ui.item.remove();
                        removeLast = false;
                    }
                    scope.value = values();
                    scope.$apply();
                    element.parent().css("overflow","hidden");
                    var dimension = ui.item.data().dimension;
                    ui.item.toggleClass("invalid", !isValidType(dimension))
                    message();
                    $rootScope.$broadcast("update");
                }

                scope.$watch('value', function (value){
                    if (!value.length) {
                        element.find('li').remove();
                    }
                    message();
                })

                function onReceive(e,ui) {
                    var dimension = ui.item.data().dimension;
                    removeLast = hasValue(dimension);
                    if (!scope.multiple && scope.value.length) {
                        var found = false;
                        element.find('li').each(function (i,d) {
                            if ($(d).data().dimension.key == scope.value[0].key && !found) {
                                $(d).remove();
                                found = true;
                                removeLast=false;
                            }
                        })
                    }
                    scope.value = values();
                    ui.item.find('span.remove').click(function(){  ui.item.remove(); onRemove(); })
                }

                function onRemove(e,ui) {
                    scope.value = values();
                    scope.$apply();
                        $rootScope.$broadcast("update");
                    }
                    function values(){
                        if (!element.find('li').length) return [];
                        var v = [];
                        element.find('li').map(function (i,d){
                            v.push($(d).data().dimension);
                        })
                        return v;
                    }
                    function hasValue(dimension){
                        for (var i=0; i<scope.value.length;  i++) {
                            if (scope.value[i].key == dimension.key) {
                                return true;
                            }
                        }
                        return false;
                    }

                    function isValidType(dimension) {
                        if (!dimension) return;
                        return scope.types.map(function (d){ return d.name; }).indexOf(dimension.type) != -1;
                    }

                    function message(){
                        var hasInvalidType = values().filter(function (d){ return !isValidType(d); }).length > 0;
                        scope.messageText = hasInvalidType
                            ? "You should only use " + scope.types.map(function (d){ return d.name.toLowerCase() + "s"; }).join(" or ") + " here"
                            : "Drag " + scope.types.map(function (d){ return d.name.toLowerCase() + "s"; }).join(", ") + " here";
                    }

                }
            }
    })

    .directive('draggable', function () {
        return {
            restrict: 'A',
            scope:false,
            link: function postLink(scope, element, attrs) {
                scope.$watch('metadata', function(metadata) {
                    if(!metadata.length) element.find('li').remove();
                    element.find('li').draggable({
                        connectToSortable:'.dimensions-container',
                            helper : 'clone',
                        revert: 'invalid',
                        start : onStart
                    })
                })
                function onStart(e, ui) {
                    ui.helper.addClass("dropped");
                    ui.helper.css('z-index','100000');
                }
            }
        }
    })

    .directive('group', function () {
        return {
            restrict: 'A',
            link: function postLink(scope, element, attrs) {
                scope.$watch(attrs.watch, function (watch){
                    var last = element;
                    element.children().each(function(i, o){
                        if((i) && (i) % attrs.every == 0) {
                            var oldLast = last;
                            last = element.clone().empty();
                            last.insertAfter(oldLast);
                        }
                        $(o).appendTo(last)
                    })
                },true)
            }
        };
    })

    .directive('colors', function ($rootScope) {
        return {
            restrict: 'A',
            templateUrl : 'templates/colors.html',
            link: function postLink(scope, element, attrs) {
                scope.scales = [
                    {
                        type : 'Ordinal (categories)',
                        value : d3.scale.ordinal().range(raw.divergingRange(1)),
                        reset : function(domain){ this.value.range(raw.divergingRange(domain.length || 1)); },
                        update : ordinalUpdate
                    },
                    {
                        type : 'Linear (numeric)',
                        value : d3.scale.linear().range(["#f7fbff", "#08306b"]),
                        reset : function(){ this.value.range(["#f7fbff", "#08306b"]); },
                        update : linearUpdate
                    }
                ];

                function ordinalUpdate(domain) {
                    if (!domain.length) domain = [null];
                    this.value.domain(domain);
                    listColors();
                }

                function linearUpdate(domain) {
                    domain = d3.extent(domain, function (d){return +d; });
                    if (domain[0]==domain[1]) domain = [null];
                    this.value.domain(domain).interpolate(d3.interpolateLab);
                    listColors();
                }

                scope.setScale = function() {
                    scope.option.value = scope.colorScale.value;
                    scope.colorScale.reset(scope.colorScale.value.domain());
                    $rootScope.$broadcast("update");
                }

                function addListener(){
                    scope.colorScale.reset(scope.colorScale.value.domain());
                    scope.option.on('change', function (domain){
                        scope.option.value = scope.colorScale.value;
                        scope.colorScale.update(domain);
                    })
                }

                scope.colorScale = scope.scales[0];
                scope.$watch('chart', addListener)
                scope.$watch('colorScale.value.domain()',function (domain){
                    scope.colorScale.reset(domain);
                    listColors();
                }, true);

                function listColors(){
                    scope.colors = scope.colorScale.value.domain().map(function (d){
                        return { 
                            key: d, 
                            value: scope.colorScale.value(d).charAt(0) == '#' ? scope.colorScale.value(d) : '#' + scope.colorScale.value(d) 
                        }
                    }).sort(function (a,b){
                        if (raw.isNumber(a.key) && raw.isNumber(b.key)) 
                            return a.key - b.key;
                        return a.key < b.key ? -1 : a.key > b.key ? 1 : 0;
                    })
                }

                scope.setColor = function(key, color) {
                    var domain = scope.colorScale.value.domain(),
                        index = domain.indexOf(key),
                        range = scope.colorScale.value.range();
                    range[index] = color;
                    scope.option.value.range(range);
                    $rootScope.$broadcast("update");
                }

                scope.foreground = function(color){
                    return d3.hsl(color).l > .5 ? "#000000" : "#ffffff";
                }

                scope.$watch('option.value', function (value){
                    if(!value) scope.setScale();
                })
            }
        };
    })