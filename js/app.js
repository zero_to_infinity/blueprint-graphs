'use strict';

angular.module('blueprint', [
  'ngRoute',
  'ngAnimate',
  'ngSanitize',
  'blueprint.filters',
  'blueprint.services',
  'blueprint.directives',
  'blueprint.controllers',
  'ui',
  'ngFileUpload'
])

.config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider) {
  $routeProvider.when('/', {templateUrl: 'partials/main.html', controller: 'BlueprintCtrl'});
  $routeProvider.otherwise({redirectTo: '/'});
  $locationProvider.html5Mode(true);
}]);
